/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions
} from 'react-native';

import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import Data from './components/data'
const {width ,height} = Dimensions.get('window');
export default class App extends Component<{}> {
  constructor(props){
    super(props)
    this.state ={
      region: {
        latitude:null,
        longitude:null,
        latitudeDelta:null,
        longitudeDelta:null,
      }
    }
  }
  
  calcDelta(lat, lon , accuracy){
    const oneDegreeOfLongitudInMeters = 111.32
    const circumference =(40075/360)
    const latDelta = accuracy * (1 /(Math.cos(lat) * circumference))
    const lonDelta = (accuracy / oneDegreeOfLongitudInMeters)

    this.setState({
      region: {
        latitude:lat,
        longitude:lon,
        latitudeDelta:0.0922,
        longitudeDelta:0.0421,
      }
    })
  }
  componentDidMount(){
    navigator.geolocation.getCurrentPosition(position =>{
        const lat = position.coords.latitude
        const lon = position.coords.longitude
        const accuracy = position.coords.accuracy

        this.calcDelta(lat,lon,accuracy)
    })
  }
  marker(){
    return{
      latitude: this.state.region.latitude,
      longitude:this.state.region.longitude
    }
  }
  render() {
    return (
      <View style={styles.container}>
          {this.state.region.latitude?<MapView
            provider={PROVIDER_GOOGLE}
            style={StyleSheet.absoluteFillObject}
              initialRegion={
                this.state.region
              }  
        >
        <MapView.Marker
          coordinate={this.marker()}
          title="teste de construção de mapa"
          description="descrição do mapa criado"
        />
        </MapView>:null}
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map:{
    flex:1,
    width: width
  }
});
