import React, { Component } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  StyleSheet,
  TouchableHighlight,
  ScrollView,
  ListView,
  TextInput,
  Alert,
  Picker,
} from 'react-native';
 
 
export default class data extends Component{
  constructor(props){
      super(props)
 
      this.state = {
        radioTake: '#D32F2F',
        radioDelivery: 'transparent',
        receiptway: false,
        timetotake: '',
        focused: '',
        borderColor: 'rgba(0, 0, 0, .12)',
        takename: '',
        takeidentity: '',
        name: '',
        identity: '',
        phone: '',
        changemoney: '',
        street: '',
        neighborhood: '',
        housenumber: '',
        reference: '',
        userid: '',
        user: null,
      }
    
  }
  render(){
    return (
        <View style={styles.receiptwayform}>
 
      <View style={styles.obstextinputcontainer}>
        <Text style={styles.obsinfotext}>EM QUANTO TEMPO BUSCARÁ A PIZZA?</Text>
        <Picker style={styles.picker} onBlur={this.onBlur}
          onFocus={() => this.onFocus('obs')} selectedValue={this.state.timetotake} onValueChange={(value) => this.setState({timetotake: value})}>
          <Picker.Item label="30 minutos" value="30min" />
          <Picker.Item label="45 minutos" value="45min" />
          <Picker.Item label="1 hora" value="1hora" />
        </Picker>
      </View>
 
      <View style={styles.obstextinputcontainer}>
        <Text style={styles.obsinfotext}>NOME:</Text>
        <TextInput
          value={this.state.takename}
          onChangeText={(val) => this.setState({takename: val})}
          placeholder= 'Ex: Sérgio Costa'
          onBlur={this.onBlur}
          onFocus={() => this.onFocus('name')}
          style={styles.obstextinput} />
      </View>
 
      <View style={styles.obstextinputcontainer}>
        <Text style={styles.obsinfotext}>CPF:</Text>
        <TextInput
          value={this.state.takeidentity}
          onChangeText={(val) => this.setState({takeidentity: val})}
          placeholder= 'Ex: 000.000.000-00'
          onBlur={this.onBlur}
          onFocus={() => this.onFocus('identity')}
          style={styles.obstextinput} />
      </View>
 
      <TouchableHighlight underlayColor='transparent' style={styles.tipebuttoncontainer} onPress={this.showUserDataTake}>
        <Text style={{color: '#FFFFFF', fontWeight: 'bold'}}>PREENCHER CAMPOS COM MEUS DADOS</Text>
      </TouchableHighlight>
 
     </View>
    );
  }
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  toolbarcontainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 8,
  },
  imgsearch: {
    width: 24,
    height: 24
  },
  storename: {
    color: '#D32F2F',
    fontSize: 22,
    fontWeight: 'bold',
    margin: 8,
    textAlign: 'center'
  },
  info: {
    textAlign: 'center',
  },
  typeperson: {
    alignItems: 'center',
  },
  typepersoninfo: {
    textAlign: 'center',
    color: 'rgba(0, 0, 0, .87)',
  },
  receiptwayview: {
    flexDirection: 'row',
    padding: 8,
    justifyContent: 'space-between',
  },
  receiptwayform: {
    flex: 1,
    padding: 16,
  },
  radiobutton: {
    flex: 1,
  },
  radiobuttoncontainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  outer: {
    width: 24,
    height: 24,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: '#D32F2F',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inside: {
    width: 12,
    height: 12,
    borderRadius: 6,
  },
  typereceipt: {
    marginLeft: 8,
  },
  picker: {
    backgroundColor: 'transparent',
    borderWidth: 0,
  },
  obstextinputcontainer: {
    marginTop: 8,
    borderColor: 'rgba(0, 0, 0, .12)',
    borderBottomWidth: 1,
  },
  obstextinput: {
    backgroundColor: 'transparent',
    borderWidth: 0,
  },
  obsinfotext: {
    fontSize: 14,
    color: 'rgba(0, 0, 0, .87)',
  },
  bottombarcontainer: {
    backgroundColor: '#FFF',
    borderColor: 'rgba(0, 0, 0, .12)',
    borderTopWidth: 1,
    padding: 8,
    height: 56,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  orderinfotext: {
    color: 'rgba(0, 0, 0, .87)',
    fontSize: 16,
  },
  checkorderbutton: {
    backgroundColor: '#D32F2F',
    padding: 8,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tipebuttoncontainer: {
    marginTop: 8,
    backgroundColor: '#FF9100',
    padding: 8,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
  }
});